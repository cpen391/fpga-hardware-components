module avalon_hw_key (
    input logic clk, input logic rst_n,
    input logic [1:0] addr, input logic rd_en,
    output logic [31:0] data_out
);
    always @(*) begin
        case (addr)
            2'b00 : data_out <= 32'h207cf1cb;
            2'b01 : data_out <= 32'h7e63e0d8;
            2'b10 : data_out <= 32'h39ddaefc;
            2'b11 : data_out <= 32'h763c017a;
        endcase
    end
endmodule