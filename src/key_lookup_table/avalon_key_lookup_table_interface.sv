module avalon_key_lookup_table_interface (
    input logic clk, input logic rst_n,  
    input logic [2:0] addr, input logic [9:0] switches,
    input logic rd_en, input logic wr_en, input logic [31:0] data_in, 
    output logic wait_request, output logic [31:0] data_out
);

    /* key_lookup_table signals */
    logic [31:0] s0_high, s0_low, s1_high, s1_low;
    logic regen, lookup, table_rdy, table_rst_n;
    logic [31:0] lookup_out;
    /* state */
    enum { READY, REGEN1, REGEN2, LOOKUP1, LOOKUP2, TABLE_RESET, PULSE_WR, DEBUG1 } current_state;

    key_lookup_table kl_table(
        .clk(clk), .rst_n(table_rst_n), .switches(switches),
        .s0({s0_high, s0_low}), .s1({s1_high, s1_low}),
        .regen(regen), .lookup(lookup), .lookup_out(lookup_out),
        .rdy(table_rdy)
    );

    always @(posedge clk) begin
        if (~rst_n) begin
            regen <= 0;
            lookup <= 0;
            data_out <= 32'hDEADBEEF;
            wait_request <= 1;
            table_rst_n <= 1;
            current_state <= READY;
        end else begin
            case (current_state)
                READY : begin
                    casex ({rd_en, wr_en, addr})
                        /* write functions */
                        {1'b0, 1'b1, 3'b000} : begin
                            s0_high <= data_in;
                            wait_request <= 0;
                            current_state <= PULSE_WR;
                        end
                        {1'b0, 1'b1, 3'b001} : begin
                            s0_low <= data_in;
                            wait_request <= 0;
                            current_state <= PULSE_WR;
                        end
                        {1'b0, 1'b1, 3'b010} : begin
                            s1_high <= data_in;
                            wait_request <= 0;
                            current_state <= PULSE_WR;
                        end
                        {1'b0, 1'b1, 3'b011} : begin
                            s1_low <= data_in;
                            wait_request <= 0;
                            current_state <= PULSE_WR;
                        end
                        /* read functions */
                        {1'b1, 1'b0, 3'b100} : begin        // regen table request
                            regen <= 1;
                            current_state <= REGEN1;
                        end
                        {1'b1, 1'b0, 3'b101} : begin        // table lookup request
                            lookup <= 1;
                            current_state <= LOOKUP1;
                        end
                        {1'b1, 1'b0, 3'b110} : begin        // reset table request
                            table_rst_n <= 0;
                            current_state <= TABLE_RESET;
                        end
                        /* DEBUG READ FUNCTIONS */
                        {1'b1, 1'b0, 3'b000} : begin
                            data_out <= s0_high;
                            wait_request <= 0;
                            current_state <= PULSE_WR;
                        end
                        {1'b1, 1'b0, 3'b001} : begin
                            data_out <= s0_low;
                            wait_request <= 0;
                            current_state <= PULSE_WR;
                        end
                        {1'b1, 1'b0, 3'b010} : begin
                            data_out <= s1_high;
                            wait_request <= 0;
                            current_state <= PULSE_WR;
                        end
                        {1'b1, 1'b0, 3'b011} : begin
                            data_out <= s1_low;
                            wait_request <= 0;
                            current_state <= PULSE_WR;
                        end
                    endcase
                end
                TABLE_RESET : begin
                    table_rst_n <= 1;
                    wait_request <= 0;
                    data_out <= 32'hDEDEDEDE;       //reset request magic number   
                    current_state <= PULSE_WR;
                end
                REGEN1 : begin
                    regen <= 0;
                    current_state <= REGEN2;
                end
                REGEN2: begin
                    if (table_rdy) begin
                        wait_request <= 0;
                        data_out <= 32'hABABABAB;   //regen request magic number
                        current_state <= PULSE_WR;
                    end
                end
                LOOKUP1 : begin
                    lookup <= 0;
                    current_state <= LOOKUP2;
                end
                LOOKUP2 : begin
                    data_out <= lookup_out;
                    wait_request <= 0;
                    current_state <= PULSE_WR;
                end
                PULSE_WR : begin
                    wait_request <= 1;
                    current_state <= READY;
                end
            endcase
        end
    end
endmodule