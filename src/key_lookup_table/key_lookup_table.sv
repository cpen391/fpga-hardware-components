module key_lookup_table(
    input logic clk, input logic rst_n, input logic [9:0] switches,
    input logic [63:0] s0, input logic [63:0] s1, input logic regen,
    input logic lookup, output logic [31:0] lookup_out, output logic rdy
);

    /* 4 kb memory for table */
    logic [31:0] table_mem[1023:0];
    /* local signals */
    logic s_valid, xoroshiro_next;
    logic [63:0] xoroshiro_out;
    logic [9:0] regen_index;
    /* state */
    enum { READY, REGEN_START1, REGEN_START2, REGEN } current_state;

    xoroshiro_128_interface xoroshiro(
        .clk(clk), .s_valid(s_valid), .next(xoroshiro_next), 
        .s0_in(s0), .s1_in(s1), .out(xoroshiro_out)
    );
 
    always @(posedge clk) begin
        if (~rst_n) begin
            s_valid <= 0;
            xoroshiro_next <= 0;
            regen_index <= 0;
            rdy <= 1;
            current_state <= READY;
        end else begin
            case (current_state)
                READY : begin
                    if (regen) begin
                        s_valid <= 1;
                        xoroshiro_next <= 1;
                        rdy <= 0;
                        current_state <= REGEN_START1;
                    end else if (lookup) begin
                        lookup_out <= table_mem[switches];
                    end
                end
                REGEN_START1 : begin
                    s_valid <= 0;
                    current_state <= REGEN_START2;
                end
                REGEN_START2 : begin
                    current_state <= REGEN;
                end
                REGEN : begin
                    if (regen_index == 10'b1111111111) begin
                        rdy <= 1;
                        xoroshiro_next <= 0;
                        current_state <= READY;
                    end
                    table_mem[regen_index] <= xoroshiro_out[61:32]; // TODO
                    regen_index += 1;
                end
            endcase
            
        end
    end
    
endmodule