module xoroshiro_128_interface(
    input logic clk, input logic s_valid, input logic next,
    input logic [63:0] s0_in, input logic [63:0] s1_in,
    output logic [63:0] out
);
    logic [63:0] s0, s1, s0_next, s1_next;

    xoroshiro_128_next core(.s0_in(s0), .s1_in(s1), .s0_out(s0_next), .s1_out(s1_next));

    always @(posedge clk) begin
        if (s_valid) begin
            s0 <= s0_in;
            s1 <= s1_in;
        end else if (next) begin
            out <= s0 + s1;
            s0 <= s0_next;
            s1 <= s1_next;
        end
    end

endmodule


module xoroshiro_128_next(
    input logic [63:0] s0_in, input logic [63:0] s1_in,
    output logic [63:0] s0_out, output logic [63:0] s1_out
);
    logic [63:0] s0_rotl, s1_rotl, s1_xor;

    rotl rotl0(.x(s0_in), .k(24), .out(s0_rotl));
    rotl rorl1(.x(s1_xor), .k(37), .out(s1_rotl));

    assign s1_xor = s1_in ^ s0_in;
    assign s0_out = s0_rotl ^ s1_xor ^ (s1_xor << 16);
    assign s1_out = s1_rotl;
endmodule