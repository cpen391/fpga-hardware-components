module rotl(input logic [63:0] x, input logic [31:0] k, output logic [63:0] out);
    assign out = (x << k) | (x >> (64 - k));
endmodule