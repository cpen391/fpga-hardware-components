`timescale 1ps / 1ps

module tb_key_lookup_table();
    logic clk, rst_n;
    logic [9:0] switches;
    logic [63:0] s0, s1;
    logic regen, rdy, lookup;
    logic [31:0] lookup_out;

    key_lookup_table dut(.*);

    always #10 clk = ~clk;
    initial begin
        clk = 0;
        switches = 0;
        regen = 0;
        rst_n = 0;

        #20; rst_n = 1;
        s0 = {32'h0, 32'h4A3C6C};
        s1 = {32'h0, 32'hF3DCE};
        regen = 1;
        @(posedge clk);
        regen = 0;

        @(posedge rdy)
        switches = 10'b0000000001;
        lookup = 1;
        @(posedge clk); lookup = 0;
        #100;

        $stop;
    end
endmodule