`timescale 1ps / 1ps

module tb_avalon_key_lookup_table_interface();
    logic clk, rst_n, rd_en, wr_en, wait_request;
    logic [2:0] addr;
    logic [9:0] switches;
    logic [31:0] data_in, data_out;

    avalon_key_lookup_table_interface dut(.*);

    always #10 clk = ~clk;
    initial begin
        clk = 0;
        rst_n = 0;
        wr_en = 0;
        @(posedge clk);
        rst_n = 1;

        addr = 0;
        rd_en = 1;
        @(negedge wait_request)
        rd_en = 0;
        #95;

        addr = 6;
        rd_en = 1;
        @(posedge wait_request)
        addr = 0;
        rd_en = 0;
        wr_en = 1;
        data_in = 0;
        @(posedge wait_request)
        addr = 1;
        wr_en = 1;
        data_in = 32'h4A3C6C;
        @(posedge wait_request)
        addr = 2;
        wr_en = 1;
        data_in = 0;
        @(posedge wait_request)
        addr = 3;
        wr_en = 1;
        data_in = 32'hF3DCE;
        @(posedge wait_request)
        wr_en = 0;

        addr = 4;
        rd_en = 1;
        @(posedge wait_request)

        addr = 5;
        @(posedge wait_request)
        rd_en = 0;

        #100
        $stop;
    end

endmodule
