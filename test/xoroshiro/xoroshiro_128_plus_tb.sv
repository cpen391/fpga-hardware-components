`timescale 1ps / 1ps

module xoroshiro_128_plus_tb();
    logic clk, s_valid, next;
    logic [63:0] s0_in, s1_in, out;

    xoroshiro_128_interface dut(.*);

    always #10 clk = ~clk;
    initial begin
        clk = 0;
        s_valid = 1;
        next = 0;
        s0_in = 4865132;
        s1_in = 998862;

        #20;
        s_valid = 0;
        next = 1;

        #4000;
    end

endmodule